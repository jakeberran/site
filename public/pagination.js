// to use in next function, hides one page
function hide(num) {
    document.getElementById("p"+num.toString()).style.display='none';
    document.getElementById("P"+num.toString()).style='background-color: var(--class-item-bg); color: black';
    document.getElementById("Q"+num.toString()).style='background-color: var(--class-item-bg); color: black';
}

//results in just the shown page to be shown
function show(num) {
    document.getElementById("p"+num.toString()).style.display='block';
    document.getElementById("P"+num.toString()).style='background-color: #A48AE5 !important; color: white'
    document.getElementById("Q"+num.toString()).style='background-color: #A48AE5 !important; color: white'
}

//updates the page and sets the current number
function update(num) {
    for(i=1; i<=pages; i++) {
        if (i !== num) {
            hide(i);
        }
    }
    show(num);
    current = num;
}

function nextPage() {
    current++;
    if (current > 3) {
        current = 3;
    }
    update(current);
}

function prevPage() {
    current--;
    if (current < 1) {
        current = 1;
    }
    update(current);
}

//for hovering over the info icon

function infoHover(element) {
    element.setAttribute('src', 'https://jakeberran.com/media/icons/info_selected.png');
}

function infoUnhover(element) {
    element.setAttribute('src', 'https://jakeberran.com/media/icons/info_unselected.png');
}

//for hovering over the info icon

function linkHover(element) {
  element.setAttribute('src', 'https://jakeberran.com/media/icons/info_selected.png');
}

function linkUnhover(element) {
  element.setAttribute('src', 'https://jakeberran.com/media/icons/link.png');
}

//hide for elements, not whole pages

function toggleHide(id) {
    var x = document.getElementById(id);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }