function set_given_length() {
  if (document.getElementById('is_scale_length').checked) {
      document.getElementById('given_length').placeholder = "Scale Length (L)";
  }
  else {
      if (document.getElementById('is_smallest_fret').checked) {
          document.getElementById('given_length').placeholder = "Smallest Fret Distance";
      }
  }
}

function roundTo(num, places) {
  result = Math.round(num*10**places)/10**places
  return result;
}

function toScaleLength(divisions, smallest, num_of_frets) { //for [divisions]-EDO, calculates scale length based on the length of smallest fret, and number of frets
  let scale_length = smallest/((1/(2**((num_of_frets-1)/divisions)))-(1/(2**(num_of_frets/divisions))));
  return scale_length;
}

function distance(length, total, current) {
  let distance = roundTo(length * (1-(1/(2 ** (current/total)))), 4);
  return distance;
}

function arrayToHTMLTable(arr) {
  let tablecode = '';
  for (let i = 0; i < arr.length; i++) {
      tablecode += '<tr>\n';
      for (let j = 0; j < arr[i].length; j++) {
          tablecode += '<th>'+arr[i][j]+'</th>\n';
      }
      tablecode += '</tr>'
  }
  return tablecode
}

function makeTable() {
  //START VARIABLE CALCULATING
  var divisions = document.getElementById('divisions').value; //the scale length is later because it now may depend on smallest fret

  var unit_text = 'in';
  if (document.getElementById('mm').checked) {
      unit_text = 'mm';
  }
  
  
  var num_of_frets;
  var frets_entry = document.getElementById('frets').value;
  if (frets_entry !== "") { //if the frets is filled in then set to that, otherwise set to two octaves
      num_of_frets = frets_entry;
  }
  else {
      num_of_frets = divisions * 2;
  }

  var given_length = document.getElementById('given_length').value;
  
  
  var scale_length;
  if (document.getElementById('is_scale_length').checked) { //set the scale length, or else calculates it if given the smallest fret
      scale_length = given_length;
  }
  else {
      scale_length = toScaleLength(divisions, given_length, num_of_frets);
  }
  //END VARIABLE CALCULATING
  
  let fret_array = [
      ['Fret # k', `Distance d_k (${unit_text})`, `Distance from Previous (${unit_text})`]
  ];
  
  
  for (let i = 1; i <= num_of_frets; i++) {
      let dist = distance(scale_length, divisions, i);
      let d_fromlast = 'N/A';
      if (i > 1) {
          d_fromlast = roundTo(dist-fret_array[fret_array.length-1][1], 4);
      }
      fret_array.push([String(i), String(dist), String(d_fromlast)]);
  }
  //just need to make a long html string, and then set that to the innerHtml
  tablecode = arrayToHTMLTable(fret_array);
  document.getElementById("fret_table").innerHTML = tablecode;
  document.getElementById("scale_length_header").innerHTML = `Scale Length: ${roundTo(scale_length, 4)} ${unit_text}`;
  document.getElementById("divisions_header").innerHTML = `Tones Per Octave: ${divisions}`;
  return false;
}