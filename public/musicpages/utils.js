var range = 1 / 500; // GDrive iframes are 450px
var vw = range * Math.min(window.innerWidth, window.innerHeight);

document.documentElement.style.setProperty('--gd-scale', `${vw}`);

window.addEventListener('resize', () => {
  document.documentElement.style.setProperty('--gd-scale', `${range * Math.min(window.innerWidth, window.innerHeight)}`);
});

function copyToClipboard(copyText, element) {
   // Copy the text inside the text field
  navigator.clipboard.writeText(copyText);

  // Alert the copied text
  x = element.lastChild
  x.style.display = 'inline'
  x.textContent = ' Copied!';
  setTimeout(() => {
    x.style.display = 'none'
  }, 3000)
}