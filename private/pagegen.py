import sys

#PAGES
#olderelec, oldelec, elec, fcsm, fclg, sdt, sm, lg, perf, improv

#all pages currently
pages = ['olderelec', 'oldelec', 'elec', 'fcsm', 'fclg', 'sdt', 'sm', 'lg', 'perf', 'improv', 'misc']

#UNCOMMENT THIS AND SET FOR SINGLE PAGE
#pages.append("oldelec")

page_titles = {"olderelec":"Older Electronic Music",
"oldelec": "Old Electronic Music",
"elec": "Electronic Music",
"fcsm":"Fun Classical (Small)",
"fclg":"Fun Classical (Large)",
"sdt":"Solos/Duos/Trios",
"sm":"Small Ensemble",
"lg":"Large Ensemble",
"perf":"Performances",
"improv":"Improvisations",
"misc":"Misc Projects"}

directories = {"olderelec":"https://jakeberran.com/media/audio/electronic13_16/",
"oldelec": "https://jakeberran.com/media/audio/electronic16_18/",
"elec": "https://jakeberran.com/media/audio/electronic19_24/",
"fcsm":"https://jakeberran.com/media/audio/class_fun/",
"fclg":"https://jakeberran.com/media/audio/class_fun/",
"sdt":"https://jakeberran.com/media/audio/classical/",
"sm":"https://jakeberran.com/media/audio/classical/",
"lg":"https://jakeberran.com/media/audio/classical/",
"perf":"https://jakeberran.com/media/audio/performance/",
"docs":"https://jakeberran.com/media/docs/",
"improv":"https://jakeberran.com/media/audio/improv/",
"misc":"https://jakeberran.com/media/audio/misc/"}

attribute_formats = {'year': '<b>Year:</b> {}', 'duration': """<b>Duration:</b> {} minutes""", "instrumentation":'<b>Instrumentation:</b> {}', 'description':'{}'}



def write(current_page):
    #create a list out of that data
    import csv

    with open('private/Music_Directory.csv', newline='') as f: #encoding='cp1252'
        print(f)
        reader = csv.reader(f)
        xall = list(reader)
        headers = xall.pop(0)
        xall.reverse()
        xall.insert(0,headers)

    #print(xall) #as a test

    columnOf = {}
    for i in range(len(headers)):
        columnOf[headers[i]] = i
    #print(columnOf)

    #create the sublist, just the songs for current page
    x = []
    for line in xall:
        if line[columnOf['page']] == current_page:
            x.append(line)
    #print(x)

    num_of_pages = len(x)//23 + 1
    per_page = len(x)//num_of_pages

    #figure out num of pages

    file_text = ""
    file_text += """<!DOCTYPE html>
    <html>
        <head>
            <title>Jake Berran | """+page_titles[current_page]+"""</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
            <link href="../style.css" rel="Stylesheet"  type="text/css" />
            <link rel="icon" href="https://jakeberran.com/media/icons/siteicon.png">
            <script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
            <script>
                $(function() {
                    $('#header').load("../header.html");
                    $('#footer').load("../footer.html");
                });
            </script>
            <script src="../pagination.js">    
            </script>
            <script>
                var pages = """+str(num_of_pages)+"""; //sets the number of pages for this page
            </script>
            <script> // TODO FIX THIS SCRIPT
                window.onload = () => {
                    console.log(window.location.search);
                    const urlParams = new URLSearchParams(window.location.search);
                    console.log(Array.from(urlParams.values()));
                    const nodesToShow = Array.from(urlParams.values()).map(id => document.getElementById(id.toLowerCase()));

                    // SHOW ONLY THE WORKS IN THE QUERY STRING
                    if (nodesToShow.length > 0) {
                        // HIDE EVERYTHING BUT THOSE IDS
                        const allWorks = Array.from(document.querySelectorAll(".work"));
                        // for (i = 0; i < allWorks.length; i++) {
                        //     allWorks[i].style.display = "none";
                        // }
                        // const dividers = Array.from(document.querySelectorAll(".work-divider"));
                        // for (i = 0; i < dividers.length; i++) {
                        //     dividers[i].style.display = "none";
                        // }
                        
                        // HIGHLIGHT THE ONES IN THE QUERY STRING
                        for (i = 0; i < nodesToShow.length; i++) {
                          nodesToShow[i].classList.add("work-highlight");
                          nodesToShow[i].scrollIntoView({
                              behavior: 'smooth',
                              block: 'center',
                              inline: 'center'
                          });
                        }

                        // document.getElementById("h1space").innerHTML = ""; // was for hiding

                    } 
                };
            </script>
        </head>
    <body>
    <div id="wrapper">
        <header>
            <div id="header"></div>
        </header>
        <main>
            <h1 id="h1space">"""+page_titles[current_page]+"""</h1>"""
            
    if num_of_pages > 1:        
        file_text += """<div class="pagination">
        <a href="#" onclick="return prevPage();">&laquo;</a>"""

        for i in range(1, num_of_pages+1):
            file_text += '\n<a id="P'+str(i)+'" href="#" onclick="return update('+str(i)+');">'+str(i)+'</a>'

        file_text += '\n<a href="#" onclick="return nextPage();">&raquo;</a></div>'

    row = 0 #ultimately we are iterating over the rows, but for making the HTML we iterate over the pages and songs per page
    for i in range(1, num_of_pages+1): #iterates over the pages
        file_text += '\n<div class="content-page" id="p'+str(i)+'"><div class="table">'
            
        for j in range(per_page): #iterates over the songs in the page
            
            #create a simple dictionary to return the data for the current row
            row_data = {}
            for header in headers:
                row_data[header] = x[row][columnOf[header]]

            #stop writing HTML for the works if there is no song
            #it should be on i = num_of_pages here so it will break out of both loops
            if row_data['index'] == "":
                break

            if 'exclude' not in row_data['settings']: #skip everything if marked to exclude
                
                #TODO rewrite a lot of this as format strings

                #put in horizontal break if not first song on page
                if j > 0:
                    file_text += '<hr class="work-divider">'

                #set up the div for the whole work and the title / subtitle
                this_song_id = row_data['title'].replace(" ", "_").lower()
                file_text += """\n<div class="work" id='""" + this_song_id + """'><span class="song-info"><h3>""" + row_data['title'].replace('""','"') + (f""" <u>{row_data['subtitle']}</u>""" if row_data['subtitle'] else "") + '</h3>'
                
                #add the PDF access if nonempty
                if row_data['doc']:
                    if row_data['doc'][:7] == "preview":
                        file_text += '  <a href="'+directories['docs']+row_data['doc']+'" target="_blank" rel="noopener noreferrer"><img src="https://jakeberran.com/media/icons/preview.png" height="40px"/></a>'
                    else:
                        file_text += '  <a href="'+directories['docs']+row_data['doc']+'" target="_blank" rel="noopener noreferrer"><img src="https://jakeberran.com/media/icons/pdf.png" height="32px"/></a>'
                
                #add the buy button
                if row_data['data-checkout']:
                    buytext = 'Buy'
                    if 'free' in row_data['settings']:
                        buytext = 'Free'
                    file_text += '''\n<button class="cp-button" data-seller="jakeberran" data-checkout="'''+row_data['data-checkout']+'''">'''+buytext+'''</button>'''

                #build up the description
                full_description = ""
                for attribute in attribute_formats:
                    if row_data[attribute]:
                        if attribute == 'description': #preps the description
                            desc = row_data[attribute]
                            desc = '<p>' + desc.replace('\n\n', '</p><p>').replace('\n \n', '</p><p>') + '<p>'
                            desc = desc.replace('\n', '<br>')
                            full_description += desc + "<br/>"
                        else:    
                            full_description += attribute_formats[attribute].format(row_data[attribute]) + "<br/>" #formats the description
                
                #if there is one, put in the toggle button and description
                if full_description:
                    file_text += """\n<input type="image" height="32px" src="https://jakeberran.com/media/icons/info_unselected.png" onmouseover="infoHover(this);" onmouseout="infoUnhover(this);" onclick="toggleHide('"""+row_data["title"]+'''_description');" />'''
                
                #put in the copy to clipboard icon
                file_text += f'''\n<a style="cursor: pointer; text-decoration: none;" onclick="copyToClipboard('https:\/\/jakeberran.com/musicpages/{current_page}.html?song={this_song_id}', this);" onmouseover="linkHover(this);" onmouseout="linkUnhover(this);"><img src="https://jakeberran.com/media/icons/link.png" height="24px"/><span></span></a>'''
                
                if full_description:
                    file_text += '''\n<div style="display: none;" id="'''+row_data['title']+'''_description">\n<br/>'''+full_description+'''</div>'''

                file_text += "\n</span>\n"

                #sets up the players span
                file_text += '<span class="song">'

                #embedded players
                if row_data['embed']: #embed
                
                    embed = row_data['embed']#.replace('""', '"') #before csv module
                    embed = embed.replace('width="560" height="315"', "")
                    embed = embed.replace('iframe ', 'iframe class="video" ', 1)
                    file_text += '<div class="vidcontainer">' + embed + '</div>'

                    if row_data['embed2']: #embed
                        embed = row_data['embed2']#.replace('""', '"') #before csv module
                        embed = embed.replace('width="560" height="315"', "")
                        embed = embed.replace('iframe ', 'iframe class="video" ', 1)
                        file_text += '<div class="vidcontainer">' + embed + '</div>'

                #emedded audio
                if row_data['link']:
                    if row_data['embed'] == "" or 'showall' in row_data['settings']:
                        file_text += '''<iframe class="gdrive" frameborder="0" style="border:3px solid black; overflow: hidden; position: relative;" src="'''+row_data['link'].replace('/edit', '/preview').replace('/view', '/preview')+'''"></iframe>'''
                file_text += '\n</span></div>\n'

            #increment the row
            row += 1
        
        #close the div for the table, then the whole pagination page
        file_text += '\n</div></div>'

    #add in the pagination stuff, if necessary
    if num_of_pages > 1:
        file_text += '\n<div class="pagination"><a href="#" onclick="return prevPage();">&laquo;</a>'

        for i in range(1, num_of_pages+1):
            file_text += '\n<a id="Q'+str(i)+'" href="#" onclick="return update('+str(i)+');">'+str(i)+'</a>'

        file_text += """\n<a href="#" onclick="return nextPage();">&raquo;</a>
                </div>
                <script>update(1);</script>"""

    #close out the HTML    
    file_text += """</main>
        <footer>
            <div id="footer"></div>
        </footer>
        
    </div>

    <script type="text/javascript" src="https://checkoutpage.co/js/overlay.js" defer></script>  
    <script type="text/javascript" src="utils.js" ></script>  
    </body>

    </html>"""

    #open the target file, clear it and replace it with the big file_text string
    target = open('public/musicpages/'+current_page+'.html', 'w', encoding='utf-8')
    target.truncate(0)
    target.write(file_text)
    #data.close()
    target.close()


#write the about pages (including the about section of the main page)
def writeAbout():
    f = open('private/texts/bios.txt', 'r') 
    text = f.read()
    f.close()

    text = text.replace('\n', '')
    num = text.count('%')//2
    bios = text.split('%')

    html = '''<!DOCTYPE html>
    <html>
    <head>
        <title>Jake Berran | About</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link href="style.css" rel="Stylesheet"  type="text/css" />
        <link rel="icon" href="https://jakeberran.com/media/icons/siteicon.png">
        <script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
        <script>
            $(function() {
                $('#header').load("header.html");
                $('#footer').load("footer.html");
            });
        </script>
    </head>
    <body>


    <div id="wrapper">
        <header>
          <div id="header"></div>
        </header>

    <main>
        <section class="blog">
        <div>
            <h1>About</h1>
            <p><a href="announcements.html">See announcements and updates &gt;&gt;&gt;</a></p>
            
            '''

    for i in range(2, num):
        header = bios[2*i]
        body = bios[2*i+1]
        html += '<h2>'+header+'</h2>\n'
        html += '<p>'+body+'</p>\n'
        if i == 2:
            html += "<img src='https://jakeberran.com/media/pictures/headshot.jpg' alt='Headshot' width='75%' class='center'>\n"

    html += '''</div></section>
        </main>
        
        
        <footer>
        <div id="footer"></div>
        </footer>
        
    </div>
        
    </body>

    </html>'''

    #print(html)

    target = open('public/about.html', 'w', encoding='utf-8')
    target.truncate(0)
    target.write(html)
    target.close()


    # HOMEPAGE
    source = open('public/index.html', 'r', encoding='utf-8')
    text = source.read()
    source.close()

    n = text.find('<!--ABOUT-->') + len('<!--ABOUT-->')
    n2 = text.find('<a href="about.html">')
    #print(text[:n])
    text = text[:n] + '\n<p>'+bios[1] + ' ' + text[n2:]
    #print(newtext)

    # GETTING RID OF ANNOUNCEMENT SECTION, UNCOMMENT TO UNDO
    # m = text.find('<a href="announcements.html">') + len('<a href="announcements.html">')
    # m2 = text.find('<!--END ANNOUNCEMENT-->')
    # text = text[:m] + 'Announcement: ' + bios[3] + '>>>' + text[m2:]
    
    target = open('public/index.html', 'w', encoding='utf-8')
    target.truncate(0)
    target.write(text)


#call the commands
for page in pages:
    write(page)

writeAbout()