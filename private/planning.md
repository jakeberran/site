# PLANNING DOCUMENT
=================
!! Get a PDF of the swirling pulling score
!! Break out the performance information and the program notes and extra description, instead of just one big description field.

Put swirling pulling up on github?

Javascript generate and download a .csv for the fret calculator
Separate pages for projects (add music python, on github, add documentation) and old/fun songs

IMPLEMENT BOOTSTRAP

add tears with benhur when done


Add meta descriptions to all? pages <meta name="description" content="Put your description here.">

HAVE A TEXT BOX TO ENTER A SONG (WITH DROP DOWN/SUGGESTIONS) AND IT WILL GO TO THAT
Have different fields for electronic things, so I can have more accurate descriptions

shop:
list ballade for sale, checkout page

style:
https://www.jqueryscript.net/other/Cross-Platform-Audio-Player-tinyPlayer.html
space between video and audio
and hover over that 'a' should overlay a tint over the element. the social media icons?
customize the checkout page CSS

pagegen
later on, maybe create objects and use those? to handle multimovement works differently. or use json? and javascript?
nah that loads slower

LATER:
have a search feature (maybe just the google search?)
    somehow have tags on each one?
    by instrumentation
    by mood?
    by title obviously
    by year/age

Use Vue/Nuxt to make the site instead?